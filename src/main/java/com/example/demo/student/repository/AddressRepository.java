package com.example.demo.student.repository;

import com.example.demo.student.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository
        extends JpaRepository <Address, Long>{
}
