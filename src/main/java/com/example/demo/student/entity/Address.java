package com.example.demo.student.entity;

import javax.persistence.*;

@Entity
@Table(name="address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int street_no;
    private String city;
    private String state;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "address")
    private Student student;

    public Address() {
    }

    public Address(Long id, int street_no, String city, String state) {
        this.id = id;
        this.street_no = street_no;
        this.city = city;
        this.state = state;
    }

    public Address( int street_no, String city, String state) {
        this.street_no = street_no;
        this.city = city;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getStreet_no() {
        return street_no;
    }

    public void setStreet_no(int street_no) {
        this.street_no = street_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street_no=" + street_no +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
