package com.example.demo.student.config;

import com.example.demo.student.entity.Address;
import com.example.demo.student.entity.Student;
import com.example.demo.student.repository.AddressRepository;
import com.example.demo.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        return args -> {

            Student student1 = new Student(
                    "Student1",
                    "student1@gmail.com",
                    LocalDate.of(2000, 1, 12)
            );

            studentRepository.saveAll(List.of(student1));

        };

    }

}
