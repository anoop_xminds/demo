package com.example.demo.student.controller;

import com.example.demo.student.repository.StudentRepository;
import com.example.demo.student.service.StudentService;
import com.example.demo.student.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    private StudentRepository studentRepository;

    @GetMapping("/getStudent")
    public List<Student> getStudents(){
        return studentService.getStudents();
    }



}
