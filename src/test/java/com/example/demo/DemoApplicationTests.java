package com.example.demo;

import com.example.demo.student.controller.StudentController;
import com.example.demo.student.entity.Student;
import com.example.demo.student.repository.StudentRepository;
import com.example.demo.student.service.StudentService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private StudentService studentService;

	@MockBean

	@Autowired
	private StudentRepository studentRepository;

	@InjectMocks
	StudentController studentController;

	@Test
	public void testRegisterNewStudent(){
		Student student = new Student();
		student.setName("Student1");
		student.setEmail("student1@gmail.com");
		when(studentRepository.save(ArgumentMatchers.any(Student.class))).thenReturn(student);
		Student created = studentService.addNewStudent(student);
		assertThat(created.getName()).isSameAs(student.getName());
		verify(studentRepository).save(student);
	}

}
